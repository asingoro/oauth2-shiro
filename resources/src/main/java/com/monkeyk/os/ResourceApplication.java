package com.monkeyk.os;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Mr.Yangxiufeng on 2017/12/5.
 * Time:14:48
 * ProjectName:oauth2-shiro
 */
@SpringBootApplication
public class ResourceApplication {
    public static void main(String[] args) {
        SpringApplication.run(ResourceApplication.class);
    }
}
