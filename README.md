# oauth2-shiro-spring-boot

由于当前项目打算用OAuth2.0实现，笔者对花了点时间对[oauth2-shiro](http://https://gitee.com/mkk/oauth2-shiro)进行改造，如下：
- 采用springboot进行全面的改造，移除xml配置
- 去除用户登录、添加等代码，保留纯粹的认证中心
- 所以oauth2-shiro-spring-boot 是从 [oauth2-shiro](http://https://gitee.com/mkk/oauth2-shiro) fork，基于Spring boot 改造的提供一个轻量的OAUTH2应用框架。并根据不同的应用场景提供不同的实现(如web场景,移动设备).

相比spring-oauth-server, oauth2-oltu具有如下特点:
-  **更加透明** 每一步实现都有可以查看的, 更容易理解的源代码, 一目了然
- **更多的可自定义与可扩展** 不管是ERROR返回信息的内容或格式, 都可根据需要自定义, 对请求参数,处理细节等可添加更多的具体实现
- **可读性更强**  由于Shiro, Oltu 没有Spring Security,spring-security-oauth2 的门槛高, 所有代码都是常用的Controller或Java Bean实现各项业务, 更可读,更易于理解
- **模块化** 得益于Oltu的模块化设计, 将authz, resources分开成不同的模块, 使用时可根据实际需要将二者合并在一个项目中或拆分为不同的模块

# 主要技术
- spring boot 1.5.8.RELEASE
- apache oltu 1.0.2
- alibaba druid 1.1.6
- shiro 1.2.3
- MySQL 5.6

# 项目模块说明
oauth2-shiro项目使用模块化开发, 以实现"高内聚, 低耦合"目标, 更符合实际项目需要; 分为三个模块: authz, core 与 resources, 具体说明如下:
-  **authz** 实现使用各类grant_type去获取token业务逻辑----获取access_token
-  **core** 将公共部分提取到该模块中, 减少重复代码, 保证一致性, 如定义ClientDetails, AccessToken;  authz, resources 模块都依赖于该模块
-  **resources** 资源管理模块,将受OAUTH保护的资源(URI)放在这里----使用access_token

# 如何使用
- 创建MySQL数据库(如数据库名 oauth2_shiro), 并运行相应的SQL脚本(脚本文件位于others/database目录)

# 里程碑
- 之后会把本项目采用redis的改造给放出来
- 2017-12-06 完成Springboot 改造


# 支持的 grant_type
说明 oauth2-shiro 项目支持的grant_type(授权方式)与功能

- authorization_code -- 授权码模式(即先登录获取code,再获取token)
- password-- 密码模式(将用户名,密码传过去,直接获取token)
- refresh_token -- 刷新access_token
- implicit(token)> -- 简化模式(在redirect_uri 的Hash传递token; Auth客户端运行在浏览器中,如JS,Flash)
- client_credentials -- 客户端模式(无用户,用户向客户端注册,然后客户端以自己的名义向'服务端'获取资源)

# 使用说明

分别运行authz和resouces工程。

### 获取Token
以password模式为例：

- 输入client

![输入图片说明](https://gitee.com/uploads/images/2017/1207/092608_679d50bd_394692.png "1.png")

- 输入username、password

![输入图片说明](https://gitee.com/uploads/images/2017/1207/092636_85cbb43e_394692.png "2.png")

- 用access_token获取资源

![输入图片说明](https://gitee.com/uploads/images/2017/1207/092651_90d75d88_394692.png "3.png")