package com.monkeyk.os.config;

import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Mr.Yangxiufeng on 2017/12/4.
 * Time:16:42
 * ProjectName:oauth2-shiro
 */
@Configuration
public class OauthConfig {
    @Bean(name = "oAuthIssuer")
    public OAuthIssuer getOAuthIssuer(){
        OAuthIssuerImpl oAuthIssuer = new OAuthIssuerImpl(getMd5Generator());
        return oAuthIssuer;
    }
    @Bean
    public MD5Generator getMd5Generator(){
        MD5Generator md5Generator = new MD5Generator();
        return md5Generator;
    }
}
