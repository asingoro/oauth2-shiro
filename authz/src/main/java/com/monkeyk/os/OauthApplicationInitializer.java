package com.monkeyk.os;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Created by Mr.Yangxiufeng on 2017/12/4.
 * Time:15:42
 * ProjectName:oauth2-shiro
 */
public class OauthApplicationInitializer extends SpringBootServletInitializer{
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(OauthApplication.class);
    }
}
